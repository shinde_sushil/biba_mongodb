<?php

namespace App\Console\Commands;

use App\Entry;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Config;
use Mockery\Exception;
use Twitter;
use App\SocialPostLog;
use App\SocialPost;

class TweetPull extends Command
{

    protected $signature = 'tweet:fetch {old? : Fetch only older data from twitter server}';

    protected $description = 'This command will fetch all tweets from twitter servers';

    public function __construct()
    {
        parent::__construct();
    }

//  function to get normal response from twitter servers
    private function getData($hashTag, $count, $format)
    {
        $GLOBALS["max_request_count"] += 1;
        return Twitter::getSearch(["q" => $hashTag, "count" => $count, "format" => $format, "include_entities" => "1", 'since' => config('app.start_date')]);
    }

//  function to get older tweets from twitter servers
    private function getNextPage($hashTag, $count, $format, $max_id)
    {
        $GLOBALS["max_request_count"] += 1;
        if (self::isWindows()) {
            $max_id = bcsub($max_id, 1);
        } else {
            $max_id -= 1;
        }
        return Twitter::getSearch(["q" => $hashTag, "count" => $count, "format" => $format, "include_entities" => "1", "max_id" => $max_id, 'since' => config('app.start_date')]);
    }

//   This function will give latest or most recent tweets
    private function getLatestPage($hashTag, $count, $format, $since_id)
    {
        $GLOBALS["max_request_count"] += 1;
        return Twitter::getSearch(["q" => $hashTag, "count" => $count, "format" => $format, "include_entities" => "1", "since_id" => $since_id]);
    }

// This function will check OS type. Windows server does not support direct subtracting of large integer so we need to use bcmath library.
    private function isWindows()
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') {
            return true;
        } else {
            return false;
        }
    }

//store data in database
    private function storeData($response)
    {
        $entry = SocialPostLog::saveEntry($response);
        $progress_bar = $this->output->createProgressBar(count($response["statuses"]));
        $post_saved = 1;
        foreach ($response["statuses"] as $row) {
            SocialPost::savePost($row, self::mediaUrl($row));
            Entry::saveEntry($row, self::mediaUrl($row));
            $entry->post_end_id = $row["id_str"];
            $entry->total_post_saved = $post_saved;
            $post_saved++;
            $progress_bar->advance();
            $entry->save();
        }
        $entry->status = "completed";
        $entry->save();
        $progress_bar->finish();
    }

//  This function will give all media urls from single tweets.
    private function mediaUrl($row)
    {
        $media["type"] = NULL;
        $media["img_url"] = NULL;
        $media["img_thumb"] = NULL;
        $media["video_url"] = NULL;
        if (isset($row["entities"]["media"])) {
            $media_array = $row["entities"]["media"][0];
            $media["type"] = $media_array["type"];
            $media["img_url"] = $media_array["media_url"] . ":large";
            $media["img_thumb"] = $media_array["media_url"] . ":thumb";
        } else {
            $media["type"] = "text";
        }
        if (isset($row["extended_entities"]["media"])) {
            $media_array = $row["extended_entities"]["media"][0];
            if ($media_array["type"] == "video" || $media_array["type"] == "animated_gif") {
                $media["type"] = $media_array["type"];
                $media["video_url"] = $media_array["video_info"]["variants"][0]["url"];
            }
        }
        return $media;
    }

//This function will save latest entries
    private function saveLatestEntries($hashTag, $count, $format)
    {
        $this->info("fetching latest tweets");
        $since_id = SocialPostLog::getMaxEntry();
        $batch_count = 1;
        $response = self::getLatestPage($hashTag, $count, $format, $since_id);
        if (sizeof($response["statuses"]) == 0) {
            $this->line('No latest data found, Now storing older data');
            return true;
        }
        while (true) {
            $this->line("\n" . $batch_count . " batch");
            $batch_count++;
            $entry = SocialPostLog::saveEntry($response);
            $progress_bar = $this->output->createProgressBar(count($response["statuses"]));
            $post_saved = 1;
            foreach ($response["statuses"] as $row) {
                if (SocialPost::idExist($row["id_str"])) {
                    $this->line("\nStored all latest tweets, looking for older tweets now");
                    $entry->status = "completed";
                    $last_entry = end($response["statuses"]);
                    $entry->post_end_id = $last_entry["id_str"];
                    $entry->save();
                    return 0;
                }
                SocialPost::savePost($row, self::mediaUrl($row));
                Entry::saveEntry($row, self::mediaUrl($row));
                $entry->post_end_id = $row["id_str"];
                $entry->total_post_saved = $post_saved;
                $entry->save();
                $post_saved++;
                $progress_bar->advance();
            };
            $entry->status = "completed";
            $entry->save();
            $progress_bar->finish();
            $response = self::getNextPage($hashTag, $count, $format, SocialPostLog::getMaxEndId());
            self::maxCount();
        }
        return true;
    }

//Check if we reached max request count.
    private function maxCount()
    {
        if ($GLOBALS["max_request_count"] >= $GLOBALS["max_request"]) {
            $this->line(" \n Max count for API request reached");
            dd();
        }
    }

// Save older tweets in database.
    private function saveOlderData($hashTag, $count, $format, $response)
    {
        $this->line('Got data from server, storing data in database');
        $batch_count = 1;
        do {
            // store all tweets first;
            $this->info("\n \n" . $batch_count . " batch ");
            $batch_count++;
            if (sizeof($response["statuses"]) == 0) {
                $this->info("\n all data fetched");
                dd();
            }
//            DB::transaction(function ($response) use ($response) {
                self::storeData($response);
//            });
            $response = self::getNextPage($hashTag, $count, $format, SocialPostLog::getMinEntry());
            self::maxCount();
        } while (true);
    }

    public function handle()
    {
        $hashTag = config('Twitter.hashTag');
        $count = config('Twitter.count');
        $format = config('Twitter.format');
        $GLOBALS["max_request"] = config('Twitter.max_request');
        $GLOBALS["max_request_count"] = 0;
        $this->line('Looking for past data in database');
        if (SocialPostLog::getMinEntry() == NULL) {
            $this->line('No past data found, starting new API request');
            $this->line('Connecting to twitter servers');
            try {
                $response = self::getData($hashTag, $count, $format);
                if (sizeof($response["statuses"]) == 0) {
                    $this->line('No data found');
                    dd();
                }
            } catch (Exception $e) {
                $this->error('API request failed');
                dd(Twitter::error());
            }
        } else {
            $this->line('Got last entry in database, connecting to servers');
            try {
                if ($this->argument('old') != "old") {
                    self::saveLatestEntries($hashTag, $count, $format);
                }
                $response = self::getNextPage($hashTag, $count, $format, SocialPostLog::getMinEntry());
                self::maxCount();
                if (sizeof($response["statuses"]) == 0) {
                    $this->line('No older data found, database is up to date');
                    dd();
                }
            } catch (Exception $e) {
                $this->error('API request failed');
                dd(Twitter::error());
            }
        }
        self::saveOlderData($hashTag, $count, $format, $response);
    }
}
