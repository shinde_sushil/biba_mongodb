<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class SocialPost extends Eloquent
{
    public static function savePost($row, $media)
    {
        $twitter_url = "http://twitter.com/";
        $new_entry = new SocialPost();
        $new_entry->network = "twitter";
        $new_entry->network_id_str = $row["id_str"];
        $new_entry->post_date = date('Y-m-d H:i:s', strtotime($row["created_at"]));
        $new_entry->url = $twitter_url . urlencode($row["user"]["screen_name"]) . "/status/" . urlencode($row["id_str"]);
        $new_entry->image = $media["img_url"];
        $new_entry->image_thumb = $media["img_thumb"];
        $new_entry->video_url = $media["video_url"];
        $new_entry->comment = utf8_encode($row["text"]);
        $new_entry->author_name = utf8_encode($row["user"]["name"]);
        $new_entry->author_image = $row["user"]["profile_image_url"];
        $new_entry->author_username = utf8_encode($row["user"]["screen_name"]);
        $new_entry->author_profile_url = $twitter_url . urlencode($row["user"]["screen_name"]);
        $new_entry->post_type = $media["type"];
        $new_entry->save();
    }

    public static function idExist($id_str)
    {
        return SocialPost::where('network_id_str', $id_str)->exists();
    }
}
