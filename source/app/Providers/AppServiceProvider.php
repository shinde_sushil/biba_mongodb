<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Cookie;
use Validator;
use Carbon\Carbon;
use App\Entry;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Validator::extend('olderThan', function ($attribute, $value, $parameters) {
            if (Cookie::get('isUserUnderAge') == true) {
                return false;
            }

            $min_age = isset($parameters[0]) ? (int)$parameters[0] : 13;
            $date_format = isset($parameters[1]) ? $parameters[1] : 'm/d/Y';
            $birth_date = \DateTime::createFromFormat($date_format, trim($value));
            $isOlderThan = $birth_date && Carbon::now()->diff($birth_date)->y >= $min_age;
            if ($birth_date && !$isOlderThan) {
                Cookie::queue('isUserUnderAge', true, 60 * 24);  // 60 * 24 = 24 hours
            }
            return $isOlderThan;
        }, 'Sorry, you\'re not eligible to enter. See <a href="' . url('entry/rules') . '" target="_blank">Official Rules</a> for details.');

        Validator::extend('maxAge', function ($attribute, $value, $parameters) {
            $max_age = isset($parameters[0]) ? (int)$parameters[0] : 120;
            $date_format = isset($parameters[1]) ? $parameters[1] : 'm/d/Y';
            $birth_date = \DateTime::createFromFormat($date_format, trim($value));
            return $birth_date && Carbon::now()->diff($birth_date)->y <= $max_age;
        }, 'Entered date of birth is invalid');

        Validator::replacer('olderThan', function ($message, $attribute, $rule, $parameters) {
            return isset($parameters[0]) ? str_replace('13', $parameters[0], $message) : $message;
        });

        Validator::extend('state', function ($attribute, $value, $parameters) {
            return Entry::isValidState($value);
        }, 'You\'re not eligible to enter due to your state of residence. See <a href="' . url('entry/rules') . '" target="_blank">Official Rules</a> for details.');

        /*
        * app/validators.php
        */

        Validator::extend('alpha_spaces', function ($attribute, $value, $parameters) {
            return preg_match('/^[\pL\s]+$/u', $value);
        }, "The :attribute may only contain letters and spaces.");
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /** Repository binds */
        $repositories = ['Entry'];

        foreach ($repositories as $repository) {
            $this->app->bind(
                "App\\Libs\\Platform\\Storage\\{$repository}\\{$repository}Repository",
                "App\\Libs\\Platform\\Storage\\{$repository}\\Eloquent{$repository}Repository"
            );
        }
    }
}
