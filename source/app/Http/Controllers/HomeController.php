<?php

namespace App\Http\Controllers;

use App\Entry;
use Illuminate\Support\Facades\Input;

class HomeController extends SiteBaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['entries'] = Entry::getApprovedEntries();
        $this->data['entriesCount'] = Entry::getApprovedEntriesCount();
        $this->data['isSourceFb'] = Input::get('source') == 'fb';
        return view('pages.home', $this->data);
    }
}