<?php
namespace App\Http\Controllers;

use App\Entry;
use App\Http\Requests\EntryStoreRequest;
use Carbon\Carbon;
use Hash;

class EntryController extends SiteBaseController
{
    public function __construct() {
        parent::__construct();
    }

    public function save(EntryStoreRequest $request) {
        try {
            $data = $request->all();
            $data['network'] = 'web';

            if (Entry::create($data)) {
                $this->ajax_response['status'] = 'success';
                $this->ajax_response['message'] = 'Success';
            }
        } catch (\Exception $ex) {
            if (env('APP_ENV') != 'production') {
                if (env('APP_DEBUG')) {
                    $this->ajax_response['message'] = $ex->getMessage();
                } else {
                    $this->ajax_response['message'] = 'An error occurred, please enable debugging in .env to see the error';
                }
            }
        }

        return $this->ajax_response;
    }

    public function get($offset) {
        $this->data['entries'] = Entry::getApprovedEntries($offset);
        $this->data['offset'] = $offset;
        
        return view('partials.entries', $this->data);
    }
}