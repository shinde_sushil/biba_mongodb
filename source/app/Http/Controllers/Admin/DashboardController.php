<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\SiteBaseController;
use Illuminate\Http\Request;

use App;
use Mail;

use Illuminate\Support\Facades\Config;
use App\Brandmovers\Charts\LineChart;
use App\Brandmovers\Charts\PieChart;
use App\Brandmovers\Charts\StateMap;
use App\Brandmovers\Charts\Stats;
use App\Brandmovers\Charts\WorldMap;
use App\Brandmovers\Ga\Model\GaModel;
use App\Brandmovers\Ga\Exceptions\GoogleAnalyticsException;

use App\Entry;

class DashboardController extends SiteBaseController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Admin dashboard
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $this->page['title'] = 'Dashboard';
        $app_start_date = date('m/d/Y', Config::get('admin.app_start_date'));

        if (Config::get('admin.app_end_date') > time()) {
            $app_end_date = date('m/d/Y');
        } else {
            $app_end_date = date('m/d/Y', Config::get('admin.app_end_date'));
        }

        $startDate = $request->input('start-date', $app_start_date);
        $endDate = $request->input('end-date', $app_end_date);
        $startDate = date_create_from_format('m/d/Y h:i:s', $startDate . ' 12:00:01');
        $endDate = date_create_from_format('m/d/Y h:i:s', $endDate . ' 11:59:59');
//		$endDate->add(new \DateInterval('P1D'));

        // ANALYTICS FROM GOOGLE
        $this->gaModel = new GaModel(Config::get('admin.ga_profile_id'));
        // Line Chart of Daily Visitors
        $options = array('chartId' => 'visitors', 'width' => '960');
        $start_date = $startDate->getTimeStamp();
        $end_date = $endDate->getTimeStamp();

        $this->db_analytics($start_date, $end_date);

        $visitors = new LineChart($this->gaModel->dailyVisits($start_date, $end_date), $options, $start_date, $end_date);
        // Basic Stats from Google Analytics
        $basic = new Stats($this->gaModel->basicStats($start_date, $end_date));

        $colors = array('#5B5C77', '#D9D676', '#90D669', '#8EAC4F', '#C98956', '#1C82D8');

        // Pie Charts
        $options_mobile = array('width' => 280, 'height' => 180, 'chart_id' => 'mobile_pie_chart', 'colors' => $colors);
        $options_browsers = array('width' => 280, 'height' => 180, 'chartId' => 'browser_pie_chart', 'colors' => $colors);
        $options_mediums = array('width' => 280, 'height' => 180, 'chartId' => 'medium_pie_chart', 'colors' => $colors);
        $options_channels = array('width' => 280, 'height' => 180, 'chartId' => 'channel_pie_chart', 'colors' => $colors);
        $mobile = new PieChart($this->gaModel->mobileVisitors($start_date, $end_date, 5), $options_mobile);
        $browsers = new PieChart($this->gaModel->getVisitorsByBrowsers($start_date, $end_date, 5), $options_browsers);
        $mediums = new PieChart($this->gaModel->getVisitorsByMedium($start_date, $end_date, 5), $options_mediums);
        $channels = new PieChart($this->gaModel->getVisitorsByChannel($start_date, $end_date, 5), $options_channels);

        // Default Country State Maps
        $states = new StateMap($this->gaModel->getVisitorsFromCountry(Config::get('admin.dash_country_filter'), $start_date, $end_date));
        $countries = new WorldMap($this->gaModel->getVisitorsByCountry($start_date, $end_date));

        return view('admin.dashboard.index', $this->data + [
                'filters' => [
                    'start-date' => $request->input('start-date', $app_start_date),
                    'end-date' => $request->input('end-date', $app_end_date) // Use Request Data since we actually add 1 to end date since we're checking for less than
                ],
            'visitors' => $visitors,
            'basic' => $basic,
            'mobile' => $mobile,
            'mediums' => $mediums,
            'channels' => $channels,
            'browsers' => $browsers,
            'states' => $states,
            'countries' => $countries,
            'page' => $this->page,
            'totalEntries' => Entry::count(),
//            'highSchoolSenior' => Entry::where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->where('student_type', 'High School Senior')->count(),
//            'collegeUndergraduate' => Entry::where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->where('student_type', 'College Undergraduate')->count(),
//            'emailsSent' => Entry::where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->where('is_email_sent', true)->count(),
            ]);
    }

    private function db_analytics($start_date, $end_date)
    {
        $start_time = date('Y-m-d 00:00:00', $start_date);
        $end_time = date('Y-m-d 23:59:59', $end_date);
//        dd($this->data['unique_entries']);
    }
}
