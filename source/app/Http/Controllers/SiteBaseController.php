<?php

namespace App\Http\Controllers;

use App\Entry;
use Illuminate\Support\Facades\Input;
use Jenssegers\Agent\Agent;
use Mail;
use Auth;
use Illuminate\Support\Facades\Request;

class SiteBaseController extends Controller {
    protected $data =  [];
    protected $ajax_response;

    public function __construct() {
        $agent = new Agent();
        $this->data['isMobile'] = $agent->isMobile();
        $this->data['current_time'] = time();
        $this->data['current_date_time'] = date('Y-m-d H:i:s', $this->data['current_time']);
        $this->data['base_url'] = \URL::to('/');
        $this->data['asset_version'] = 1.6;
        /**
         * Default ajax response assignment
         */
        $this->ajax_response = [
            'status' => 'error',
            'message' => 'An error occurred, please try again later'
        ];
    }

    public function datatable() {
        if (Auth::getUser()->role != 'admin') {
            return redirect(url('admin'));
        }
        return $this->repository->datatables();
    }

    public function store()
    {
        if (Auth::getUser()->role != 'admin') {
            return redirect(url('admin'));
        }
        $request = Request::all();
        return $this->repository->store($request);
    }

    public function update(){
        if (Auth::getUser()->role != 'admin') {
            return redirect(url('admin'));
        }
        $request = Request::all();

        return $this->repository->update($request);
    }

    public function destroy(){
        if (Auth::getUser()->role != 'admin') {
            return redirect(url('admin'));
        }
        $request = Request::all();

        return $this->repository->destroy($request);
    }

    public function edit(){
        if (Auth::getUser()->role != 'admin') {
            return redirect(url('admin'));
        }
        $request = Request::all();
        return $this->repository->edit($request);
    }


    /**
     * @param string $view - name of the email view
     * @param string $subject - subject of the email
     * @param string|array $to - email address to send email
     * @return array - returns true and empty string if email sent successfully otherwise false
     */
    public function sendEmail($view, $subject, $to) {
        $result = ['isEmailSent' => false, 'message' => 'An error occurred while sending email'];

        try {
            Mail::send($view, $this->data, function ($message) use ($subject, $to) {
                $message->subject($subject);
                $message->to($to);
            });

            if (count(Mail::failures()) == 0) {
                $result['isEmailSent'] = true;
            }
        } catch (\Exception $ex) {
            $result['message'] = $ex->getMessage();
        }

        return $result;
    }

}

//<?php
//
//namespace App\Http\Controllers;
//
//use Illuminate\Support\Facades\Request;
//use Jenssegers\Agent\Agent;
//use Mail;
//use Auth;
//use Requests;
//
//class SiteBaseController extends Controller
//{
//    protected $data = [];
//    protected $ajax_response;
//
//    public function __construct()
//    {
//        $agent = new Agent();
//        $this->data['isMobile'] = $agent->isMobile();
//        $this->data['current_time'] = time();
//        $this->data['current_date_time'] = date('Y-m-d H:i:s', $this->data['current_time']);
//        $this->data['base_url'] = \URL::to('/');
//        $this->data['asset_version'] = 1.4;
//        /**
//         * Default ajax response assignment
//         */
//        $this->ajax_response = [
//            'status' => 'error',
//            'message' => 'An error occurred, please try again later'
//        ];
//    }
//
//    public function datatable()
//    {
//        if (Auth::getUser()->role != 'admin') {
//            return redirect(url('admin'));
//        }
//
//        return $this->repository->datatables();
//    }
//
//    public function store()
//    {
//        if (Auth::getUser()->role != 'admin') {
//            return redirect(url('admin'));
//        }
//        $request = Request::all();
////        dd($request);
//        return $this->repository->store($request);
//    }
//
//
//    /**
//     * @param string $view - name of the email view
//     * @param string $subject - subject of the email
//     * @param string|array $to - email address to send email
//     * @return array - returns true and empty string if email sent successfully otherwise false
//     */
//    public function sendEmail($view, $subject, $to)
//    {
//        $result = ['isEmailSent' => false, 'message' => 'An error occurred while sending email'];
//
//        try {
//            Mail::send($view, $this->data, function ($message) use ($subject, $to) {
//                $message->subject($subject);
//                $message->to($to);
//            });
//
//            if (count(Mail::failures()) == 0) {
//                $result['isEmailSent'] = true;
//            }
//        } catch (\Exception $ex) {
//            $result['message'] = $ex->getMessage();
//        }
//
//        return $result;
//    }
//}