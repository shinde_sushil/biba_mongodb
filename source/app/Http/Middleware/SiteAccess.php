<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

class SiteAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->hasTestingAccess()) {
            return $next($request);
        } else if ($this->timeToGoLive()) {
            if (Carbon::createFromFormat('m/d/Y H:i:s', config('app.end_date_time'))->lt(Carbon::now())) {
                return response('Promotion Over');
            } else {
                return $next($request);
            }
        }

        return response('Access Denied');
    }

    /**
     * Determine if the time to go live
     * is less than now
     *
     * @return boolean
     */
    protected function timeToGoLive()
    {
        return Carbon::createFromFormat('m/d/Y H:i:s', config('app.start_date_time'))->lt(Carbon::now());
    }

    protected function hasTestingAccess()
    {
        $testing_token = '7de59935029daf1b34c8172cd2ec85e7';
        $token = Input::get('token');
        if (!empty($token) && $token == $testing_token) {
            session()->put('token', $token);
        } else {
            $token = session()->get('token');
        }

        return $token == $testing_token
        || env('APP_ENV') == 'local';
    }
}