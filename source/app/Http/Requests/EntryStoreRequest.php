<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Entry;

class EntryStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'author_name' => 'required|alpha_spaces',
            'phone' => 'required|digits:10',
            'email' => 'required|email',
            'comment' => 'required|max:140'
        ];
    }

    public function messages()
    {
        return [
            'author_name.required' => 'The name field is required.',
            'author_name.alpha_spaces' => 'The name may only contain letters and spaces.',
            'comment.required' => 'The question field is required.',
        ];
    }
}