<?php namespace App\Libs\Platform\Storage\Entry;

use App\Entry;
use League\Flysystem\Exception;
use Yajra\Datatables\Datatables;
use \DB;
use Validator;
use Illuminate\Validation\Rule;

class EloquentEntryRepository implements EntryRepository
{
    protected $model;

    /**
     * Contructor method
     *
     * @param CodeEntry $model
     */
    public function __construct(Entry $model)
    {
        $this->model = $model;
    }

    /**
     * Method to fetch all the entries from the table
     */
    public function all()
    {
        return $this->model->all();
    }

    public function datatables()
    {
//        DB::setFetchMode(PDO::FETCH_ASSOC);
//        $result = $this->model->select([
//            '_id',
//            'status',
//            'network',
//            'email',
//            'phone',
//            'comment',
//            'author_name',
//            'post_type',
//            DB::raw("DATE_FORMAT(post_date, '%m/%d/%Y %H:%i:%S') as post_date_formatted"),
//        ]);
        $result = $this->model->get();
//        $result = (array)$result;
//        dd($result);
        $datatables = Datatables::of($result);


        /* Individual search */
//        $columns = \Request::get('columns');
//        if (is_array($columns)) {
//            foreach ($columns as $column) {
//                if ($column['data'] == 'full_name') {
//                    $keyword = $column['search']['value'];
//                    $datatables->filterColumn('full_name', 'whereRaw', "CONCAT(first_name, ' ', last_name) like ?", ["%$keyword%"]);
//                }
//            }
//        }
//        dd($datatables)
//        dd($datatables->make(true));
        return $datatables->make(true);
    }

    /**
     * Method to delete an existing entry from the database
     *
     * @param int $id : id of the entry
     */
    public function delete($id)
    {
        $resource = $this->find($id);

        return $resource->delete();
    }

    /**
     * Method to fetch and return a particular record from the table by 'id'
     *
     * @param int $id : id of the entry
     */
    public function find($id)
    {
        return $this->model->find($id);
    }

    /**
     * Get a paginated listing
     *
     * @param int $limit
     * @param array $fields
     * @param array $filters
     * @param string $sort
     * @param array $with
     * @throws DBException
     */
    public function listing($limit = 25, $fields = [], $filters = [], $sort = ['id'], $with = [])
    {
        $order = 'ASC';    // default query sorting order
        $query = $this->model->newQuery();
        $tableName = $this->model->getTable();
//        dd($this->model->get());
//        $responses = $this->model->where('network', '=', 'twitter')->get();
//        return($result);
        if (!$fields) {
            $fields = [$tableName . '.*'];
        }
        if ($filters) {
            if (isset($filters['search'])) {
                $query->where($tableName . '.first_name', 'LIKE', '%' . $filters['search'] . '%');
                $query->orWhere($tableName . '.last_name', 'LIKE', '%' . $filters['search'] . '%');

            }
        }
        if ($with) {
            $with = $this->model->processWithSelects($with);
        }

        try {
            $query->with($with);
            foreach ($sort as $val) {
                $query->orderBy($val, $order);
            }
//            $fields = $this->model->get()->toArray();
//            return $fields;
            return $query->paginate($limit, $fields);
        } catch (Exception $e) {
            throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }
    }

    /**
     * Method to fetch an entry along with the respective data based on the criteria
     *
     * @param int $id
     * @param string $fields
     * @param array $with
     * @return model_object
     * @throws DBException
     */
    public function view($id, $fields = [], $with = [])
    {
        $query = $this->model->newQuery();
        $tableName = $this->model->getTable();

        if (!$fields) {
            $fields = [$tableName . '.*'];
        }
        if ($with) {
            $with = $this->model->processWithSelects($with);
            $query->with($with);
        }

        try {
            return $query->with($with)->where($tableName . '.id', '=', $id)->first($fields);
        } catch (Exception $e) {
            throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }
    }

    public function store($request){

    Validator::make($request, [
        'email' => 'nullable|email',
        'phone' => 'nullable|digits:10',
    ])->validate();

        try{
            $this->model->create([
                'status' => $request["status"],
                'network' => $request["network"],
                'comment' => utf8_encode($request["comment"]),
                'author_name' => utf8_encode($request['network']),
                'phone' => $request["phone"],
                'email' => $request["email"],
            ]);
        }catch (Exception $e){
            throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }
        return "New entry added";
    }

    public function destroy($request){
        try{
            $user = $this->model->find($request['_id']);
            $user->delete();
            return "Entry deleted";
        }catch (Exception $e){
            throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }
    }

    public function update($request){
        try{
            $user = $this->model->find($request['_id']);
            $user->status= $request["status"];
            $user->save($request);
            return "Entry updated";
        }catch (Exception $e){
            throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }
    }

    public function edit($request){

    Validator::make($request, [
    'email' => [
        'nullable',
    ],
    'phone' => [
        'nullable',
    ],
    ])->validate();
//dd('asdfdsafds');
        try{
            $user = $this->model->find($request['_id']);
            $user->status= $request["status"];
            $user->comment= $request["comment"];
            $user->email= $request["email"];
            $user->phone= $request["phone"];
            $user->network= $request["network"];
            $user->save();
            return "Entry updated";
        }catch (Exception $e){
            throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }
    }
}