<?php namespace App\Libs\Platform\Storage;

interface BaseRepository
{
    public function all();

    public function store($request);

    public function update($request);

    public function destroy($request);

    public function edit($request);

    public function find($id);

    public function listing($limit, $fields, $filters, $sort, $with);

    public function view($id, $fields, $with);
}