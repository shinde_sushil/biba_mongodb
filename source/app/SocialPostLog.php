<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class SocialPostLog extends Eloquent
{
    public static function getMaxEntry()
    {
        return self::max('post_start_id');
    }

    public static function getMaxEndId()
    {
        return self::max('post_end_id');
    }

    public static function getMinEntry()
    {
        return self::min('post_end_id');
    }

    public static function saveEntry($response)
    {

            $entry = new SocialPostLog();
            $entry->post_start_id = $response["statuses"][0]["id_str"];
            $entry->post_end_id = $response["statuses"][0]["id_str"];
            $entry->total_post_received = count($response["statuses"]);
            $entry->total_post_saved = 0;
            $entry->network = "twitter";
            $entry->save();
            return $entry;
    }
}
