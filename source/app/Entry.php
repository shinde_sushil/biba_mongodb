<?php
namespace App;

use Carbon\Carbon;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class Entry extends Eloquent
{

    protected $fillable = ['status','network', 'image', 'comment','author_name','author_image','post_type','phone','email'];

    public static function saveEntry($row, $media)
    {
        $new_entry = new Entry();
        $new_entry->network = "twitter";
        $new_entry->network_id_str = $row["id_str"];
        $new_entry->image = $media["img_url"];
        $new_entry->comment = utf8_encode($row["text"]);
        $new_entry->author_name = utf8_encode($row["user"]['screen_name']);
        $new_entry->author_image = $row["user"]["profile_image_url"];
        $new_entry->post_type = $media["type"];
        $new_entry->post_date = date('Y-m-d H:i:s', strtotime($row["created_at"]));
        $new_entry->save();
    }

    public static function getApprovedEntries($offset = 0, $limit = 6) {
        $offset = (int) $offset;
        $limit = (int) $limit;

        return self::where('status', 'approved')
            ->orderBy('id', 'DESC')->offset($offset)->limit($limit)->get();
    }

    public static function getApprovedEntriesCount() {
        return self::where('status', 'approved')->count();
    }
}