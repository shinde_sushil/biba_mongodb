<?php namespace App\Brandmovers\Charts;

use Illuminate\Support\Facades\View;

/**
 * This class generates a line chart.
 *
 * @exception
 * @author Brad Estey
 * @package BMI Component
 */
class WorldMap
{

    private $data = array();
    private $options;

    public function __construct($data = FALSE, $options = FALSE)
    {

        // Set default options.
        $this->options['chartId'] = 'worldmap';
        $this->options['width'] = 474;
        $this->options['height'] = 330;

        // Set options.
        if (is_array($options)) {
            foreach ($options as $key => $value) {
                if (array_key_exists($key, $this->options)) {
                    $this->options[$key] = $value;
                }
            }
        }

        // Set data.
        if ($data) {
            $this->data = $data;
        }
    }

    // ==============================
    // ! Accessors and Manipulators
    // ==============================

    // Get Raw Chart Data
    public function getData()
    {
        if (is_array($this->data)) {
            return $this->data;
        } else {
            return FALSE;
        }
    }


    // Set Chart Data
    public function setData()
    {
        if ($data) {
            $this->data = $data;
        }
    }


    // Get Chart ID -- clean: convert to alphanumeric, lowercase and replace spaces with underscores.
    public function getChartId($clean = FALSE)
    {
        if (!$clean) {
            return $this->options['chartId'];
        } else {
            return $this->options['chartId'];
        }
    }


    // Get Chart Display Options
    public function getOptions()
    {
        if (is_array($this->options)) {
            return $this->options;
        } else {
            return FALSE;
        }
    }

    // Set Chart Display Options
    public function setOptions($options = FALSE)
    {
        if (is_array($options)) {
            foreach ($options as $key => $value) {
                if (array_key_exists($key, $this->options)) {
                    $this->options[$key] = $value;
                }
            }
        }
    }

    // Get Chart Data by Key.
    public function __call($name, $parameters)
    {
        $name = preg_replace('/^get/', '', $name);

        // lcfirst() doesn't work on all our servers.. :(
        $name = strtolower(substr($name, 0, 1)) . substr($name, 1);

        if (array_key_exists($name, $this->options)) {
            return $this->options[$name];
        }

        return FALSE;
    }

    // Count Total Rows in Data Array.
    public function getCount()
    {
        if (is_array($this->data)) {
            return count($this->data);
        } else {
            return FALSE;
        }
    }

    // Generate HTML to show chart.
    public function generateChart()
    {
        return View::make('admin.charts.world_map', array('data' => $this));
    }
}