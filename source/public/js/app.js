var APP = APP || {};
APP.bitlyLink = 'http://bit.ly/Biba_ChangeTheQuestion';

// function load(){
// // 2. This code loads the IFrame Player API code asynchronously.
//     var tag = document.createElement('script');
//     tag.src = "https://www.youtube.com/iframe_api";
//     var firstScriptTag = document.getElementsByTagName('script')[0];
//     firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
// }
// // 3. This function creates an  (and YouTube player)
// //    after the API code downloads.
// var bannerHeight= $('.banner-wrapper').innerHeight();
// var player;
// function onYouTubeIframeAPIReady() {
//     player = new YT.Player('player', {
//         height: 'bannerHeight',
//         width: '640',
//         videoId: 'I6aXW3V-fpY',
//         events: {
//             'onReady': onPlayerReady,
//             'onStateChange': onPlayerStateChange
//         }
//     });
// }
// // 4. The API will call this function when the video player is ready.
// function onPlayerReady(event) {
//     event.target.playVideo();
// }
// // 5. The API calls this function when the player's state changes.
// //    The function indicates that when playing a video (state=1),
// //    the player should play for six seconds and then stop.
// var done = false;
// function onPlayerStateChange(event) {
//     if (event.data == YT.PlayerState.PLAYING && !done) {
//         // setTimeout(stopVideo, 9000);
//         done = true;
//     }
// }
// function stopVideo() {
//     player.stopVideo();
// }

$(document).ready(function(){
    //colorbox modal

    $("#upload-btn").click(function(){
        $.colorbox({
            rel:'group1',
            transition:"elastic",
            inline:true,
            escKey:true,
            href:"#myModal"
        });
    });

    $("#tnc").click(function(e){
        e.preventDefault();
        $.colorbox({
            transition:"elastic",
            inline:true,
            escKey:true,
            href:"#ruleModal"
        });

    });

    //loadColors();       //load colors to gallery cards

    //modal form text areay length graphics
    $( "#question" ).keyup(function() {
        $(".length").text( $(this).val().length + "/140");
    });

    //mobile navbar
    $('#nav-icon4').click(function(){
        $(this).toggleClass('open');
        $(".navbar").toggleClass('visible');
        //$("")
        $("header").toggleClass('mobile-bar');
    });
    $('.navbar ul li a').click(function () {
        if ($('#nav-icon4').hasClass('open')) {
            $(".navbar").toggleClass('visible');
            $("header").toggleClass('mobile-bar');
            $('#nav-icon4').removeClass('open');
        }
    });

    //scroll to top
    $(document).scroll(function(){
        var fadeTop = $(".main-container").offset().top - 30;
        if($(this).scrollTop() > fadeTop){
            $('header').addClass("fade-down");
        } else{
            $('header').removeClass("fade-down");
        }
        //if(!APP.isMobile == 1){
        //    onScroll();
        //}

    });

    $(".down-arrow").click(function(){
        $('html, body').animate({
            scrollTop: $(".main-container").offset().top
        }, 800);
    });

    //video image click
    $(".play-icon").click(function(){
        var bannerHeight= $('.banner-wrapper').innerHeight();
        $(".img").hide();
        $(this).hide();
        $("iframe").css({'height': bannerHeight, 'display' : 'block'});
        $("iframe").show()[0].src += "&autoplay=1";

    });

    $('#fb').on('click', function () {
        APP.fbfeedShare();
    });

    //fb & tw share
    $('#fb-home').on('click', function () {
       APP.fbShare();
    });

    $('#tw-home').on('click', function () {
        APP.twShare();
    });

    $('#tw').on('click', function () {
        var msg = encodeURIComponent("I have helped #ChangeTheQuestion. You can also join BIBA and make a little change that goes a long way!");
        APP.twShare(APP.bitlyLink, msg);
    });

    (function () {
        // Set the size of the rendered Emojis
        // This can be set to 16x16, 36x36, or 72x72
        twemoji.size = '16x16';
        // Parse the document body and // insert <img> tags in place of Unicode Emojis
        twemoji.parse(document.getElementById('gallery'));
    })();
});

//nav bar
$(document).on('click', 'a.nav', function(e){
    e.preventDefault();
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top - ($("header").height())
    }, 800);


});

//fb feed share
APP.fbfeedShare = function (link) {
    if (!link) {
        link = APP.baseUrl + '/?source=fb';
    }
    FB.ui({
        method: 'share',
        href: link
    }, function(response){});
};

//fb & tw share

APP.fbShare = function (link) {
    if (!link) {
        link = APP.baseUrl;
    }
    FB.ui({
        method: 'share',
        href: link
    }, function (response) {
    });
};


APP.twShare = function (link, message) {
    if (!link) {
        link = APP.bitlyLink;
    }
    if (!message) {
        message = encodeURIComponent(APP.shareCopy);
    }
    window.open('https://twitter.com/share?text=' + message + '&url=' + link, 'twitterwindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
};

function onScroll(event){
    var scrollPosition = $(document).scrollTop();
    $('.nav').each(function () {
        var currentLink = $(this);
        var refElement = $(currentLink.attr("href"));
        if (refElement.position().top <= scrollPosition && refElement.position().top + refElement.height() > scrollPosition) {

            $('nav ul li ').removeClass('active');
            $(this).parent().addClass('active');
        }
        else{
            $(this).parent().removeClass('active');
        }
    });
}






//boilerplate code from suntrust project
/*$(document).ready(function(){
    $('.mobile-toggle').click(function () {

        if ($('header').hasClass('open-nav')) {
            $('header').removeClass('open-nav');
        } else {
            $('header').addClass('open-nav');
        }
    });
    $('nav.main-menu li a').click(function () {
        if ($('header').hasClass('open-nav')) {
            $('header').removeClass('open-nav');
        }
    });

    var header_height=$("header").innerHeight();
    $(".home_banner,.thankyou-section,.winners-section,.register,.register-thankyou,.rules-section").css('margin-top',header_height);
    $(".info-copy a").on('click', function(event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            var top=$(hash).offset().top - header_height/1.5;
            $('html, body').animate({
                scrollTop:top
            },600);
        }
    });


    var jump=function(e)
    {
        if (e){
            e.preventDefault();
            var target = $(this).attr("href");
        }else{
            var target = location.hash;
        }
        $('html,body').animate(
            {
                scrollTop: $(target).offset().top - (header_height+10)
            },800);
    }

    $('.error_msg a[href^=#] ').bind("click", jump);
    if (location.hash) {
        $('html, body').scrollTop(0).show();
        jump();
    }

});*/


var APP = APP || {};
var ngApp = angular.module('siteApp', ['entryController']);
ngApp.filter('isEmpty', function () {
    var bar;
    return function (obj) {
        for (bar in obj) {
            if (obj.hasOwnProperty(bar)) {
                return false;
            }
        }
        return true;
    };
}).service('ngSharedProperties', function() {
    var underAge = false;

    return {
        getUnderAge: function() {
            return underAge;
        },
        setUnderAge: function(value) {
            underAge = value;
        }
    }
}).directive('dateFormat', function() {
    var allowedKeys = [8, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57];
    return {
        restrict: 'A',
        link: function(scope, elm, attrs, ctrl) {
            elm.on('keypress', function(event) {
                var keyCode = (event.which) ? event.which : event.keyCode;
                if (allowedKeys.indexOf(keyCode) != -1) {
                    return true;
                } else {
                    event.preventDefault();
                    return false;
                }
            }).on('keyup', function(event) {
                var keyCode = (event.which) ? event.which : event.keyCode;
                var ignoreKeys = [8];
                var value = elm.val();

                if (ignoreKeys.indexOf(keyCode) == -1 && value
                    && (value.length == 2 || value.length == 5)) {

                    elm.val(value + '/');
                }
            });
        }
    }
}).directive('compileHtml', function ($compile) {
    return function (scope, element, attrs) {
        scope.$watch(
            function(scope) {
                return scope.$eval(attrs.compileHtml);
            },
            function(value) {
                element.html(value);
                $compile(element.contents())(scope);
            }
        );
    };
}).directive('zipCode', function(){
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue ? inputValue.replace(/[^\d]/g,'') : null;

                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
}).directive('trimSpaces', function(){
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {

            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue ? inputValue.replace(/[\s]/g, '') : null;

                if (transformedInput!=inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
}).directive('usPhone', function(){
    var allowedKeys = [8, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57];
    return {
        restrict: 'A',
        link: function(scope, elm, attrs, ctrl) {
            elm.on('keypress', function(event) {
                var keyCode = (event.which) ? event.which : event.keyCode;
                if (allowedKeys.indexOf(keyCode) != -1) {
                    return true;
                } else {
                    event.preventDefault();
                    return false;
                }
            }).on('keyup', function(event) {
                var keyCode = (event.which) ? event.which : event.keyCode;
                var ignoreKeys = [8];
                var value = elm.val();

                if (ignoreKeys.indexOf(keyCode) == -1 && value
                    && (value.length == 3 || value.length == 7)) {

                    elm.val(value + '-');
                }
            });
        }
    }
});
angular.module('entryController', [])
    .controller('enter', function($scope, $http) {
        $scope.user = {};
        $scope.errors = {};
        $scope.isError = false;
        $scope.isSubmitting = false;

        $scope.$watchCollection('errors', function() {
            if (!angular.equals($scope.errors, {})) {
                $scope.isError = true;
            } else {
                $scope.isError = false;
            }
        });

        $scope.submit = function() {
            $scope.errors = {};
            if ($scope.isSubmitting) {
                return;
            }

            $scope.isSubmitting = true;

            $http({
                method: 'POST',
                url: APP.baseUrl + '/entry',
                data: $.param($scope.user),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'X-CSRF-TOKEN': APP.csrfToken
                }
            }).success(function(response) {
                if (response.status == 'success') {
                    $.colorbox({href:"#thanksModal",  inline: true});
                } else {
                    if (response.csrfToken) {
                        APP.csrfToken = response.csrfToken;
                        $scope.isSubmitting = false;
                        $scope.submit();
                        return;
                    }
                    $.colorbox.resize();
                    setTimeout(alert('success'), 3000);
                    $scope.errors['error'] = [response.message];
                    $scope.isSubmitting = false;
                }
            }).error(function(response, status) {
                if (status === 422) {
                    // set empty error message for duplicate errors
                    var errors = [];
                    angular.forEach(response, function(v, k) {
                        if (errors.indexOf(v[0]) != -1) {
                            response[k] = [''];
                        } else {
                            errors.push(v[0]);
                        }
                    });
                    $scope.errors = response;
                    setTimeout(function() {
                        $('#myModal').colorbox.resize();
                    }, 100);
                } else {
                    $scope.errors['error'] = ['An error occurred, please try again later'];
                }
                $scope.isSubmitting = false;

            });
        };
    }).controller('loadMore', function($scope, $http) {
        $scope.user = {};
        $scope.errors = {};
        $scope.isError = false;
        $scope.isloadingMore = false;
        $scope.hideLoadMore = false;

        $scope.loadMore = function() {
            var $gallery = $('#gallery .row');
            $scope.errors = {};
            if ($scope.isloadingMore) {
                return;
            }

            $scope.isloadingMore = true;

            $http({
                method: 'get',
                url: APP.baseUrl + '/entry/' + $gallery.find('.col').length,
                data: $.param($scope.user),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'X-CSRF-TOKEN': APP.csrfToken
                }
            }).success(function(response) {
                if (response.trim() !== '') {
                    $gallery.append(twemoji.parse(response));
                } else {
                    $scope.hideLoadMore = true;
                }
                $scope.isloadingMore = false;
            }).error(function(response, status) {
                $scope.isloadingMore = false;
            });
        };
    });