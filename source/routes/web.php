<?php

Route::get('/', 'HomeController@index');
Route::post('/entry', 'EntryController@save');
Route::get('/entry/{offset}', 'EntryController@get');

/* [START] Admin Routes */
Route::group(['namespace' => 'Admin'], function() {
    Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function() {
        Route::get('dashboard', 'DashboardController@index');

        Route::post('entry/datatable', 'EntryController@datatable');
        Route::post('entry/store', 'EntryController@store');
        Route::post('entry/update', 'EntryController@update');
        Route::post('entry/destroy', 'EntryController@destroy');
        Route::post('entry/edit', 'EntryController@edit');

        Route::resource('entry', 'EntryController', ['only' => [
            'index'
        ]]);
    });

    Route::get('admin/', 'AuthController@getIndex');
    Route::post('admin/', 'AuthController@postIndex');
    Route::any('admin/logout', 'AuthController@getLogout');
});
/* [END] Admin Routes */

//test route. Delete view and route after testing. 
/*Route::get('/t',function (){
   \Illuminate\Support\Facades\Artisan::call('tweet:fetch');
});

Route::get('/test', function () {
    $data = DB::table('entries')->get();
    return view('temp',['data' => $data]);
});*/
