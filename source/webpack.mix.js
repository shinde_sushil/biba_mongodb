let mix = require('laravel-mix');
mix.browserSync('mbiba.dev');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .scripts([
            'resources/assets/js/app.js',
            'resources/assets/js/app/app.js',
            'resources/assets/js/app/controllers/entry-controller.js'
        ], 'public/js/app.js')

    .scripts([
            'resources/assets/js/vendor/angular/angular.min.js',
            'resources/assets/js/vendor/jquery.min.js',
            'resources/assets/js/vendor/jquery.colorbox-min.js'
        ], 'public/js/vendor.js'
    )
    .sass('resources/assets/sass/app.scss', 'public/css')

    // Admin assets (styles, scripts)
    .styles([
        'resources/assets/css/vendor/bootstrap-datetimepicker.css',
        'resources/assets/css/vendor/bootstrap.min.css',
        'resources/assets/css/vendor/dataTables.bootstrap.min.css',
        'resources/assets/css/vendor/buttons.dataTables.min.css'
    ], 'public/css/admin-vendor.css')
    .styles([
        'resources/assets/css/vendor/colorbox.css'
    ], 'public/css/vendor.css')
    .sass('resources/assets/sass/admin/admin.scss', 'public/css/admin.css')
    .scripts([
        'resources/assets/js/vendor/jquery.min.js',
        'resources/assets/js/vendor/jquery-ui.min.js',
        'resources/assets/js/vendor/jquery.ui.widget.js',
        'resources/assets/js/vendor/Chart.min.js',
        'resources/assets/js/vendor/highcharts.js',
        'resources/assets/js/vendor/moment.min.js',
        'resources/assets/js/vendor/bootstrap.min.js',
        'resources/assets/js/vendor/bootstrap-datepicker.min.js',
        'resources/assets/js/vendor/bootstrap-datetimepicker.min.js',
        'resources/assets/js/vendor/collapse.js',
        // 'resources/assets/js/vendor/collapse.js',
        'resources/assets/js/vendor/jquery.dataTables.min.js',
        'resources/assets/js/vendor/dataTables.bootstrap.min.js',
        'resources/assets/js/vendor/dataTables.buttons.min.js',
        'resources/assets/js/vendor/jszip.min.js',
        'resources/assets/js/vendor/pdfmake.min.js',
        'resources/assets/js/vendor/vfs_fonts.js',
        'resources/assets/js/vendor/jquery.colorbox-min.js',
        'resources/assets/js/vendor/buttons.html5.min.js',
        'resources/assets/js/vendor/buttons.print.min.js'
    ], 'public/js/admin/vendor.js')
    .scripts([
        'resources/assets/js/vendor/angular/angular.min.js',
        'resources/assets/js/vendor/angular/angular-datatables.min.js',
        'resources/assets/js/vendor/angular/angular-datatables.buttons.min.js',
        'resources/assets/js/admin/admin.js',
        'resources/assets/js/admin/entry/table-manage.js',
        'resources/assets/js/admin/entry/crud.js',
        'resources/assets/js/admin/app/app.js'
    ], 'public/js/admin/app.js')
    .options({
        processCssUrls: false
    });

mix.browserSync({
    proxy: 'mbiba.dev'
});
