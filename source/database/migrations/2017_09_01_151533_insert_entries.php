<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertEntries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $quetions = [
            ['comment' => 'Will yellow make my day brighter?', 'author_name' => 'Shruti'],
            ['comment' => 'Is this outfit going to keep me comfortable all day?', 'author_name' => 'Dyuti'],
            ['comment' => 'Does my outfit have the power to change my mood?', 'author_name' => 'Shreya'],
            ['comment' => 'Will this fabric make me feel closer to home?', 'author_name' => 'Riya'],
            ['comment' => 'Am I going to have a sparkling Diwali in this suit?', 'author_name' => 'Neha'],
            ['comment' => 'Is this kurta perfect for the big interview?', 'author_name' => 'Erica'],
        ];

        foreach ($quetions as $quetion) {
            $entry = new \App\Entry();
            $entry->comment = $quetion['comment'];
            $entry->author_name = $quetion['author_name'];
            $entry->network = 'web';
            $entry->status = 'approved';
            $entry->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('entries')->truncate();
    }
}
