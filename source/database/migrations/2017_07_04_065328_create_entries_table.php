<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->enum('status',array('pending','approved','rejected'))->default('pending')->index();
            $table->enum('network',array('twitter','instagram','web','admin'))->index();
            $table->string('network_id_str')->nullable()->default('')->index();
            $table->text('image')->nullable();
            $table->text('comment');
            $table->string('author_name')->index();
            $table->string('email')->nullable()->index();
            $table->string('phone')->nullable()->index();
            $table->text('author_image')->nullable();
            $table->enum('post_type',array('text','photo','video','animated_gif'))->default('text')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('entries');
    }
}
