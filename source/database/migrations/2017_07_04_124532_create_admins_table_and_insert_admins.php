<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTableAndInsertAdmins extends Migration
{
    public function up()
    {
        Schema::create('admins', function(Blueprint $table){
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->enum('role', ['admin', 'client'])->default('admin')->index();
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->rememberToken();
            $table->timestamps();
        });

        \DB::table('admins')->insert([
            'role'	=> 'admin',
            'email'   	=> 'admin@brandmovers.com',
            'password'   => Hash::make('Pu%A#UZeNEvu'),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        \DB::table('admins')->insert([
            'role'	=> 'client',
            'email'   	=> 'client@brandmovers.com',
            'password'   => Hash::make('neVuny@a#E3A'),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admins');
    }
}
