<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_posts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->enum('status',array('pending','approved','rejected'))->default('pending')->index();
            $table->enum('network',array('twitter','instagram'))->index();
            $table->string('network_id_str')->unique();
            $table->text('url');
            $table->text('image')->nullable();
            $table->text('image_thumb')->nullable();
            $table->text('video_url')->nullable();
            $table->text('comment');
            $table->string('author_name');
            $table->text('author_image');
            $table->string('author_username')->index();
            $table->text('author_profile_url');
            $table->dateTime('post_date')->index();
            $table->enum('post_type',array('text','photo','video','animated_gif'))->default('text')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('social_posts');
    }
}
