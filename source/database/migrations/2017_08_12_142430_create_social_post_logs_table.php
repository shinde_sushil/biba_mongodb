<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialPostLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_post_logs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('post_start_id')->index();
            $table->string('post_end_id')->index();
            $table->integer('total_post_received');
            $table->integer('total_post_saved');
            $table->enum('network',array('twitter','instagram'))->index();
            $table->enum('status', array('running', 'completed', 'failed'))->default('running')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('social_post_logs');
    }
}
