<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostDateInEntries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('entries', function (Blueprint $table) {
            $current_date_time = date('Y-m-d i:h:s');
            $table->datetime('post_date')->after('post_type')->index()->default($current_date_time);
        });

     /*   $db = \App\Entry::where('network', '==', 'web')->update();
        $db->postdate=
        DB::connection('mongodb')->collection('entries')->update(['UPDATE entries SET post_date=created_at WHERE network="web"']);
        DB::update('UPDATE entries INNER JOIN social_posts ON entries.network_id_str = social_posts.network_id_str'
            . ' SET entries.post_date = social_posts.post_date');*/
        $db = \App\Entry::where('network', '==', 'web')->update([
            "post_date" => DB::raw("`created_at`"),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entries', function (Blueprint $table) {
            $table->dropColumn('post_date');
        });
    }
}
