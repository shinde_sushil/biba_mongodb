<!doctype html>
<html ng-app="siteApp">
<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="csrf-token" content="{!! csrf_token() !!}">

    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Biba-Change The Question" />
    <meta property="og:url" content="{{ $base_url }}{{ $isSourceFb ? '/source=fb' : '' }}" />
    <meta property="og:site_name" content="Biba-Change the question" />
    <meta property="og:image" content="{{ $base_url }}/images/fb-share.jpg" />
    <meta property="og:description" content="{{ $isSourceFb ? config('app.feed_copy') : config('app.share_copy') }}" />
    <meta property="fb:app_id" content="{{ config('app.fb_id') }}" />

    <!-- TITLE & DESCRIPTION -->
    <title>Biba-Change The Question</title>
    <meta name="description" content="Biba is urging us to #ChangeTheQuestion. Join now to make way for a beautiful change.">
    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}?v={{ $asset_version }}" >
    <link rel="stylesheet" href="{{ asset('css/app.css') }}?v={{ $asset_version }}" >
    <link rel="icon" href="{{ $base_url }}/images/favicon.ico" type="image/x-icon" />
    <script src="//twemoji.maxcdn.com/2/twemoji.min.js?2.3.0"></script>
 </head>
<body>
@include('partials.header')
@yield('content')

{{--<div class="to-top" id="top">
    &uarr;
</div>--}}
@include('partials.footer')
@if (in_array(env('APP_ENV'), ['production', 'staging']))

    <!--GA code -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-104683853-1', 'auto');
    ga('send', 'pageview');

</script>
    <!--GA code end-->
@endif

<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '{{ config('app.fb_id') }}',
            xfbml      : true,
            version    : 'v2.6'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

@yield('script')
<script src="{{ asset('js/vendor.js') }}?v={{ $asset_version }}"></script>
<script>
    var APP = APP || {};
    APP.appEnv = '{{ env('APP_ENV') }}';
    APP.baseUrl = '{{ $base_url }}';
    APP.csrfToken = $('meta[name="csrf-token"]').attr('content');
    APP.isMobile = '{{ $isMobile }}' == '1';
    APP.shareCopy = '{{ config('app.share_copy') }}';
</script>
<script src="{{ asset('js/app.js') }}?v={{ $asset_version }}"></script>
</body>
</html>