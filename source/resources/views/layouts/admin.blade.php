<!DOCTYPE html>
<html lang="en" ng-app="adminApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <title>Biba | Admin | Brandmovers</title>
    <link rel="icon" type="image/png" href="{{ $base_url }}/favicon.ico" />
    <!-- Datatable Stylesheet -->
    <link href="{{ asset('css/admin-vendor.css') }}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/momentjs/2.11.2/moment.min.js"></script>
    <script src="{{ asset('js/admin/vendor.js') }}"></script>
    <script src="//twemoji.maxcdn.com/2/twemoji.min.js?2.3.0"></script>
    <style>html{visibility:hidden;}</style><script>if(self==top){document.documentElement.style.visibility='visible';}else{top.location=self.location;}</script>
</head>
<body>
    @if (!Auth::guest())
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                    </button>
                    <span class="navbar-brand">
                      Biba
                    </span>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('admin') }}">Dashboard</a></li>
                        @if (Auth::getUser()->role == 'admin')
                        <li><a href="{{ url('admin/entry') }}">Entries</a></li>
                        @endif
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                            <li><a href="{{ $base_url }}/admin/logout">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    @endif

    <div class="container">
        @yield('content')
    </div>

    <!-- Stylesheet -->
    @yield('stylesheet')
    <!-- Stylesheet -->
    <script>
        var APP = APP || {};
        APP.baseUrl = "<?php echo $base_url; ?>";
        APP.adminUrl = "<?php echo $base_url . '/admin'; ?>";
        APP.csrfToken = $('meta[name="csrf-token"]').attr('content');
    </script>
	<!-- Scripts -->
    <script src="{{ asset('js/admin/app.js') }}"></script>
	@yield('scripts')
</body>
</html>
