@extends('layouts.admin')

@section('content')
<script src="//www.google.com/jsapi"></script>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Date Filter</h3>
            </div>
            <div class="row" style="margin-bottom:15px">
                <form action="" method="get" class="form-inline panel-body filters-form">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="vote-start-date">Filter Date Range:</label>
                            {!! Form::text('start-date', $filters['start-date'], ['class'=>'datepicker form-control', 'size' => 12]) !!}
                            to
                            {!! Form::text('end-date', $filters['end-date'], ['class'=>'datepicker form-control', 'size' => 12]) !!}
                        </div>
                    </div>
                    <br><br><br>
                    <div class="col-md-12 text-right">
                        <button type="submit" value="search" name="submit" class="btn btn-primary">
                            <span class="glyphicon glyphicon-search"></span>
                            Search
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading"><span class="glyphicon glyphicon-signal"></span> Entry Summary</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Total Entries</th>
                                <th class="text-right">{{ number_format($totalEntries) }}</th>
                            </tr>
                            {{--<tr>--}}
                                {{--<th>High School Senior</th>--}}
{{--                                <th class="text-right">{{ number_format($highSchoolSenior) }}</th>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<th>College Undergraduate</th>--}}
{{--                                <th class="text-right">{{ number_format($collegeUndergraduate) }}</th>--}}
                            {{--</tr>--}}
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Website Traffic</h3>
            </div>
            <div class="panel-body">
                <div class="chart">
                      <?php echo $visitors->generateChart()?>
                </div>
                <dl class="dl-horizontal col-md-4 col-sm-4">
                    <dt>Total Visits:</dt>
                    <dd>{{ $basic->getVisits(TRUE)}}</dd>
                    <dt>Bounce Rate:</dt>
                    <dd>{{ $basic->getBounceRate()}}</dd>
                    <dt>Total Page Views:</dt>
                    <dd>{{ $basic->getPageviews(TRUE)}}</dd>
                </dl>
                <dl class="dl-horizontal col-md-4 col-sm-4">
                    <dt>Avg. Time On Site:</dt>
                    <dd>{{ $basic->getAvgSessionDuration()}}</dd>
                    <dt>Abs. Uniq. Visitors:</dt>
                    <dd>{{ $basic->getVisitors(TRUE)}}</dd>
                    <dt>New Visits:</dt>
                    <dd>{{ $basic->getNewVisits(TRUE)}}</dd>
                </dl>
                <dl class="dl-horizontal col-md-4 col-sm-4">
                    <dt>Pages/Visit:</dt>
                    <dd>{{ $basic->getPagesPerVisit()}}</dd>
                    <dt>% New Visits:</dt>
                    <dd>{{ $basic->getNewVisitRate(TRUE)}}</dd>
                </dl>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Visits by Channel</h3>
            </div>
            <div class="panel-body">
                <?php echo $channels->generateChart();?>
                <dl class="dl-horizontal">
                    @foreach ($channels->humanizedData() as $key => $value)
                        <dt>{{ $key}}:</dt>
                        <dd>{{ $value}}</dd>
                    @endforeach
                </dl>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Visits by Medium</h3>
            </div>
            <div class="panel-body">
               <?php echo $mediums->generateChart() ?>
                <dl class="dl-horizontal">
                    @foreach ($mediums->humanizedData() as $key => $value)
                    <dt>{{ $key}}:</dt>
                    <dd>{{ $value}}</dd>
                    @endforeach
                </dl>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Visits by Browser</h3>
            </div>
            <div class="panel-body">
                <?php echo $browsers->generateChart()?>
                <dl class="dl-horizontal">
                    @foreach ($browsers->humanizedData() as $key => $value)
                    <dt>{{ $key}}:</dt>
                    <dd>{{ $value}}</dd>
                    @endforeach
                </dl>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Mobile Platform Visitors</h3>
            </div>
            <div class="panel-body">
                <?php echo $mobile->generateChart() ?>
                <dl class="dl-horizontal">
                    @foreach ($mobile->humanizedData() as $key => $value)
                    <dt>{{ $key}}:</dt>
                    <dd>{{ $value}}</dd>
                    @endforeach
                </dl>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Visitors by Country</h3>
            </div>
            <div class="panel-body">
                 <?php echo $countries->generateChart()?>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Visitors by State</h3>
            </div>
            <div class="panel-body">
                <?php echo $states->generateChart();?>
            </div>
        </div>
    </div>
</div>


@stop


@section('scripts')
<script type="text/javascript">
    $(function () {
        $('.datepicker').datepicker();
    });
</script>
@stop
