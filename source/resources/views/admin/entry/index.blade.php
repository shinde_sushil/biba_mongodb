@extends('layouts.admin')

@section('content')
    <div id="entry-list" class="panel panel-primary">
        <div class="crud">
            <button id="new_entry" class="btn btn-primary" data-toggle="modal" data-target="#entry-modal">Add new entry</button>
        </div>
        <div class="panel-heading">
            <i class="glyphicon glyphicon-list"></i>
            <strong>
                &nbsp; Entry Listing
            </strong>
        </div>
        <br>
        <div class="container row">
            <div class="col-md-12">
                <table id="entryListing" class="table table-sm table-bordered table-striped table-hover table-condensed table-responsive">
                    <thead class="thead-inverse">
                    <tr>
                        <th class="col-md-1">ID</th>
                        <th class="col-md-1">Status</th>
                        <th class="col-md-2">Network</th>
                        <th class="col-md-1">Email</th>
                        <th class="col-md-1">Phone</th>
                        <th class="col-md-2" id="question">Question</th>
                        <th class="col-md-1">Author</th>
                        <th class="col-md-1">Post date</th>
                        <th class="col-md-2">Operation</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div id="entry-modal"  class="modal fade entry-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Create new entry</h4>
                </div>
                <div class="modal-body">
                    <form action="" class="form-group" id="entry-model-form" enctype="multipart/form-data" method="post">
                        <fieldset>
                            <label for="status">Status</label>
                            <select name="status" id="status" class="form-control" title="status" required>
                                <option value="pending">Pending</option>
                                <option value="approved">Approved</option>
                                <option value="rejected">Rejected</option>
                            </select>
                            <label for="network">Network</label>
                            <select name="network" id="network" class="form-control" title="network" required>
                                @foreach (config('app.entry_networks') as $network)
                                    <option value="{{ $network }}"{{ $network == 'admin' ? ' selected="selected"' : '' }}>{{ $network }}</option>
                                @endforeach
                            </select>
                            <label for="email">Email</label>
                            <input type="email" id="email" name="email" title="email"  class="form-control" placeholder="Email">
                            <label for="phone">Phone</label>
                            <input type="tel" id="phone" name="phone" title="phone"  class="form-control" placeholder="Phone number" max="9999999999">
                            <label for="question">Question</label>
                            <textarea id="question" name="comment" title="comment" class="form-control" required placeholder="Question" maxlength="140"></textarea>
                            {{--
                            <label for="file">Image or Video</label>
                            <input type="file" id="file" name="file" title="file" class="form-control">
                            --}}
                            <br>
                            <div class="errors" id="errors">

                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" form="entry-model-form" id="form-submit-btn">Submit</button>
                    <button type="reset" class="btn btn-warning"  form="entry-model-form">Reset <span class="glyphicon glyphicon-repeat"></span></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="edit-modal"  class="modal fade edit-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit older entry</h4>
                </div>
                <div class="modal-body">
                    <form action="" class="form-group" id="edit-model-form" enctype="multipart/form-data" method="post">
                        <fieldset>
                            <label for="status">Status</label>
                            <select name="status" id="edit-status" class="form-control" title="status" required>
                                <option value="pending">Pending</option>
                                <option value="approved">Approved</option>
                                <option value="rejected">Rejected</option>
                            </select>
                            <label for="network">Network</label>
                            <select name="network" id="edit-network" class="form-control" title="network" required>
                                @foreach (config('app.entry_networks') as $network)
                                    <option value="{{ $network }}">{{ $network }}</option>
                                @endforeach
                            </select>
                            <label for="email">Email</label>
                            <input type="email" id="edit-email" name="email" title="email"  class="form-control" placeholder="Email">
                            <label for="phone">Phone</label>
                            <input type="tel" id="edit-phone" name="phone" title="phone"  class="form-control" placeholder="Phone number" max="9999999999">
                            <label for="question">Question</label>
                            <textarea type="text" id="edit-question" name="comment" title="comment" class="form-control" required placeholder="Question" maxlength="140"></textarea>
                            {{--
                            <label for="file">Image or Video</label>
                            <input type="file" id="file" name="file" title="file" class="form-control">
                            --}}
                            <div class="edit-errors" id="edit-errors">

                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" form="edit-model-form" id="edit-submit-btn">Edit</button>
                    <button type="reset" class="btn btn-warning"  form="edit-model-form">Reset<span class="glyphicon glyphicon-repeat"></span></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('stylesheet')

@stop

@section('scripts')

@stop