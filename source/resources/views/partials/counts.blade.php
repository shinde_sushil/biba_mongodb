<section class="count-section">
    <div class="count">
        <h1>{{ $entriesCount }} questions changed</h1>
        <div class="share-btns">
            <span>Share on</span>
            <a href="" id="fb-home" class="fb-share"><img src="{{ url($base_url."/images/header/Facebook-icon-mobile.png") }}" alt=""> </a>
            <span>|</span>
            <a href="" id="tw-home" class="tw-share"><img src="{{ url($base_url."/images/header/Twitter-icon-mobile.png") }}" alt=""> </a>
        </div>
    </div>
</section>