
<div class="gallery-content" id="gallery" ng-controller="loadMore">
    <div class="row">
        @include('partials/entries')
    </div>
    @if($entriesCount > 6)
        <button ng-hide="hideLoadMore" id="load-more" ng-click="loadMore()"></button>
    @endif
</div>