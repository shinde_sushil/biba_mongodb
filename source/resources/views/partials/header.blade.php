<header class="master-header">
    <div class="logo">
        <img src="  {{ url($base_url. '/images/header/biba-logo.png')  }}">
    </div>
    <div class="mobile-tab hide">
        <div id="nav-icon4">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <nav class="navbar">
        <ul class="desk-nav">
            <li><a href="#video" class="nav"> Home</a></li>
            <li><a href="#upload-div" class="nav"> How to enter</a></li>
            <li><a href="#gallery" class="nav"> Gallery</a></li>
            <li>
                <a class="icon" href="https://www.facebook.com/BibaIndia/" target="_blank"> <img src=" {{ url($base_url.'/images/header/facebook-icon.png') }}"> </a>
                <a class="icon" href="https://www.instagram.com/bibaindia/" target="_blank"> <img src=" {{ url($base_url.'/images/header/instagram-icon.png') }}"> </a>
                <a class="icon" href="https://twitter.com/BibaIndia" target="_blank"> <img src=" {{ url($base_url.'/images/header/twitter-icon.png') }}"> </a>
            </li>
        </ul>

    </nav>
</header>
