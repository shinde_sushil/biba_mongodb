<?php
    $colors = ['#9d1421', '#920b18', '#7d0511', '#650009'];
    $c = 0;
    if (!empty($offset)) {
        $c = ($offset - 1) % 3;
    }
?>

@foreach ($entries as $entry)
    @if($c >= 4)
        @php $c = 0; @endphp
    @endif
    <div class="col s6 m4 card c{{ $entry->id }}" style="background-color: {{ $colors[$c] }}">
        <div class="question-wrapper">
            <p class="question">{{ $entry->network == 'twitter' ? utf8_decode($entry->comment) : $entry->comment }}</p>
        </div>
        <div class="share-container">
            <p class="user-name"> - {{ $entry->network == 'twitter' ? '@' : '' }}{{ $entry->author_name }}</p>
        </div>
    </div>
    @php $c++; @endphp

@endforeach