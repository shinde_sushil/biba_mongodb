<div class="video-content" id="video">
    <div class="banner-wrapper">
        <div class="play-icon"></div>
        <img class="img" src="{{ url($base_url."/images/layer1-cover.png") }}">
        <div class="down-arrow"></div>
    </div>

    <iframe width="100%" src="https://www.youtube.com/embed/I6aXW3V-fpY?rel=0&wmode=transparent&showinfo=0" frameborder="0" allowfullscreen>
    </iframe>


    <div class="main-container" id="home">
        <div class="content">
            <img class="top-design" src="{{ url($base_url.'/images/home/motif-1.png') }}">
            <div class="texts">
                <h1>#ChangeTheQuestion</h1>
                <p>A little girl poses a big question that leaves us introspecting. Are we letting beauty ideals define us?
                    Are we, as a society, giving off the right message? Are we asking the right questions? We are all shaping the
                    world with our thoughts and ideas - let's make it a beautiful one. Join us as we <strong>#ChangeTheQuestion</strong> </p>
            </div>
            <img class="bottom-design" src="{{ url($base_url.'/images/home/motif-2.png') }}">
        </div>

    </div>
</div>