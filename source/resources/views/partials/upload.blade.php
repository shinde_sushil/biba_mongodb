<div class="upload-container" id="upload-div">
    <h2>How to enter</h2>
    <div class="container">
        <div class="steps">
            <div class="step">step 1</div>
            <img src="{{ url($base_url."/images/steps/fill-form.png") }}">
            <p class="instr">Fill up your </p>
            <p class="instr">details</p>

        </div>
        <div class="steps">
            <div class="step">step 2</div>
            <img src="{{ url($base_url."/images/steps/help.png") }}">
            <p class="instr">Submit your</p>
            <p class="instr">question</p>
        </div>
        <div class="or">
            <div class="section">
                <img class="divider1" src="{{ url($base_url."/images/steps/divider-1.png") }}">
                <img class="divider1-mobile hide" src="{{ url($base_url."/images/steps/divider-1left.png") }}">
                <p>OR</p>
                <img class="divider2" src="{{ url($base_url."/images/steps/divider-2.png") }}">
                <img class="divider2-mobile hide" src="{{ url($base_url."/images/steps/divider-2right.png") }}">
            </div>
        </div>
        <div class="steps tw-wrapper">
            <div class="step">step 1</div>
            <img class="tw" src="{{ url($base_url."/images/steps/login-tw.png") }}">
            <p class="instr">Login via</p>
            <p class="instr">Twitter</p>
        </div>
        <div class="steps">
            <div class="step">step 2</div>
            <img src="{{ url($base_url."/images/steps/tw-ctq.png") }}">
            <p class="instr">Tweet with</p>
            <p class="instr">#ChangeTheQuestion</p>
        </div>

    </div>
    <div class="upload">
        <a id="upload-btn" class="group1" href="#myModal">CHANGE THE QUESTION</a>
    </div>




    {{--Popup window--}}
    <div class="hide">
        <div id="myModal" class="modal-content" ng-controller="enter">
            <h2>Tell us about yourself</h2>
            <form>
                <div class="row">
                    <div class="col m3 s3">
                        <label for="">Name*:</label>
                    </div>
                    <div class="col m9 s9">
                        <input type="text" name="userName" ng-model="user.author_name">
                    </div>
                </div>
                <div class="row">
                    <div class="col m3 s3">
                        <label for="email">Email*:</label>
                    </div>
                    <div class="col m9 s9">
                        <input type="email" name="email" ng-model="user.email">
                    </div>
                </div>
                <div class="row">
                    <div class="col m3 s3">
                        <label for="">Phone*:</label>
                    </div>
                    <div class="col m9 s9">
                        <input type="text" name="phone" ng-model="user.phone">
                    </div>
                </div>
                <div class="row">
                    <div class="col m3 s3">
                        <label for="">Enter your Question*:</label>
                    </div>
                    <div class="col m9 s9">
                        <textarea rows="6" maxlength="140" id="question" ng-model="user.comment"></textarea>
                        <p class="length">@{{ 140 - user.comment }}/140</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col m9 s9 offset-m3 offset-s3 button">
                        <button type="submit" name="add" ng-click="submit()">submit</button>
                    </div>
                </div>
                <div class="row">
                    <div class="error_msg col m9 s9 offset-m3 offset-s3" ng-cloak ng-show="errors">
                        <div ng-repeat="error in errors">@{{ error[0] }}</div>
                    </div>
                </div>

            </form>
        </div>

        <div class="modal-thankyou" id="thanksModal">
            <div class="content">
                <h3>Thank You!</h3>
                <p>You have helped</p>
                <h3 class="ctq">#ChangeTheQuestion</h3>
                <div class="share">
                    <div class="divs">
                        <p>Share on</p>
                    </div>

                    <a href="" id="fb" class="fb-share"><img src="{{ $base_url.'/images/header/facebook-icon.png'}}"></a>
                    <a href="" id="tw" class="tw-share"> <img src="{{ $base_url.'/images/header/twitter-icon.png'}}"></a>
                </div>
            </div>
        </div>
    </div>

</div>