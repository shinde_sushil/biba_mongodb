<footer class="footer">
    <span class="links">
        <a id="tnc" href="#ruleModal">Terms & Conditions</a>
        |
        <a href="http://brandmovers.in/" target="_blank"> Powered by Brandmovers </a>
        |
        <a href="https://www.biba.in/" target="_blank"> Visit <i>www.biba.in</i> </a>
    </span>
</footer>

{{--TnC modal--}}
<div class="hide">
    <div class="rules-modal" id="ruleModal">

        <h3>BIBA – Microsite Terms and Conditions</h3>
        <p>BIBA “Change The Question” campaign will be governed by the following Terms and Conditions:</p>
        <ul>
            <li class="rules">The campaign will be hosted on all social media channels of BIBA and a special microsite – www.bibachangethequestion.com</li>
            <li class="rules">Participation in the campaign is voluntary.</li>
            <li class="rules">Participants are advised to read the "Terms and Conditions". Participation in the campaign shall imply that the participant has duly read and agrees to adhere to and bound by the "Terms and Conditions" stated herein.</li>
            <li class="rules">You have to be 18 years and above to take part in the campaign.</li>
            <li class="rules">The best 5 entries will win Biba gift vouchers worth 1500/- each. However, participation in the contest alone does not guarantee any prizes or rewards.</li>
            <li class="rules">All questions will be reviewed before being published on the microsite.</li>
            <li class="rules">It is the sole discretion of BIBA and its associates to decide which entries are going to be published and be declared as winners.</li>
            <li class="rules">BIBA and its associates have the right to use the entries on any BIBA branding (online or offline) as they seem fit.</li>
            <li class="rules">Profanity and crude comments in any form will not be accepted and people making such entries will be blocked immediately.</li>
            <li class="rules">BIBA reserves the right to change these terms and conditions any time without prior notice. It is the participant’s responsibility to check regularly for any changes in the same.</li>
        </ul>

    </div>
</div>
