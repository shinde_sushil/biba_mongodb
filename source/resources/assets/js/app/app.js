var APP = APP || {};
var ngApp = angular.module('siteApp', ['entryController']);
ngApp.filter('isEmpty', function () {
    var bar;
    return function (obj) {
        for (bar in obj) {
            if (obj.hasOwnProperty(bar)) {
                return false;
            }
        }
        return true;
    };
}).service('ngSharedProperties', function() {
    var underAge = false;

    return {
        getUnderAge: function() {
            return underAge;
        },
        setUnderAge: function(value) {
            underAge = value;
        }
    }
}).directive('dateFormat', function() {
    var allowedKeys = [8, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57];
    return {
        restrict: 'A',
        link: function(scope, elm, attrs, ctrl) {
            elm.on('keypress', function(event) {
                var keyCode = (event.which) ? event.which : event.keyCode;
                if (allowedKeys.indexOf(keyCode) != -1) {
                    return true;
                } else {
                    event.preventDefault();
                    return false;
                }
            }).on('keyup', function(event) {
                var keyCode = (event.which) ? event.which : event.keyCode;
                var ignoreKeys = [8];
                var value = elm.val();

                if (ignoreKeys.indexOf(keyCode) == -1 && value
                    && (value.length == 2 || value.length == 5)) {

                    elm.val(value + '/');
                }
            });
        }
    }
}).directive('compileHtml', function ($compile) {
    return function (scope, element, attrs) {
        scope.$watch(
            function(scope) {
                return scope.$eval(attrs.compileHtml);
            },
            function(value) {
                element.html(value);
                $compile(element.contents())(scope);
            }
        );
    };
}).directive('zipCode', function(){
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue ? inputValue.replace(/[^\d]/g,'') : null;

                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
}).directive('trimSpaces', function(){
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {

            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue ? inputValue.replace(/[\s]/g, '') : null;

                if (transformedInput!=inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
}).directive('usPhone', function(){
    var allowedKeys = [8, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57];
    return {
        restrict: 'A',
        link: function(scope, elm, attrs, ctrl) {
            elm.on('keypress', function(event) {
                var keyCode = (event.which) ? event.which : event.keyCode;
                if (allowedKeys.indexOf(keyCode) != -1) {
                    return true;
                } else {
                    event.preventDefault();
                    return false;
                }
            }).on('keyup', function(event) {
                var keyCode = (event.which) ? event.which : event.keyCode;
                var ignoreKeys = [8];
                var value = elm.val();

                if (ignoreKeys.indexOf(keyCode) == -1 && value
                    && (value.length == 3 || value.length == 7)) {

                    elm.val(value + '-');
                }
            });
        }
    }
});