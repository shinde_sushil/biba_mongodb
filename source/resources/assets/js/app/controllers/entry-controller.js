angular.module('entryController', [])
    .controller('enter', function($scope, $http) {
        $scope.user = {};
        $scope.errors = {};
        $scope.isError = false;
        $scope.isSubmitting = false;

        $scope.$watchCollection('errors', function() {
            if (!angular.equals($scope.errors, {})) {
                $scope.isError = true;
            } else {
                $scope.isError = false;
            }
        });

        $scope.submit = function() {
            $scope.errors = {};
            if ($scope.isSubmitting) {
                return;
            }

            $scope.isSubmitting = true;

            $http({
                method: 'POST',
                url: APP.baseUrl + '/entry',
                data: $.param($scope.user),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'X-CSRF-TOKEN': APP.csrfToken
                }
            }).success(function(response) {
                if (response.status == 'success') {
                    $.colorbox({href:"#thanksModal",  inline: true});
                } else {
                    if (response.csrfToken) {
                        APP.csrfToken = response.csrfToken;
                        $scope.isSubmitting = false;
                        $scope.submit();
                        return;
                    }
                    $.colorbox.resize();
                    setTimeout(alert('success'), 3000);
                    $scope.errors['error'] = [response.message];
                    $scope.isSubmitting = false;
                }
            }).error(function(response, status) {
                if (status === 422) {
                    // set empty error message for duplicate errors
                    var errors = [];
                    angular.forEach(response, function(v, k) {
                        if (errors.indexOf(v[0]) != -1) {
                            response[k] = [''];
                        } else {
                            errors.push(v[0]);
                        }
                    });
                    $scope.errors = response;
                    setTimeout(function() {
                        $('#myModal').colorbox.resize();
                    }, 100);
                } else {
                    $scope.errors['error'] = ['An error occurred, please try again later'];
                }
                $scope.isSubmitting = false;

            });
        };
    }).controller('loadMore', function($scope, $http) {
        $scope.user = {};
        $scope.errors = {};
        $scope.isError = false;
        $scope.isloadingMore = false;
        $scope.hideLoadMore = false;

        $scope.loadMore = function() {
            var $gallery = $('#gallery .row');
            $scope.errors = {};
            if ($scope.isloadingMore) {
                return;
            }

            $scope.isloadingMore = true;

            $http({
                method: 'get',
                url: APP.baseUrl + '/entry/' + $gallery.find('.col').length,
                data: $.param($scope.user),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'X-CSRF-TOKEN': APP.csrfToken
                }
            }).success(function(response) {
                if (response.trim() !== '') {
                    $gallery.append(twemoji.parse(response));
                } else {
                    $scope.hideLoadMore = true;
                }
                $scope.isloadingMore = false;
            }).error(function(response, status) {
                $scope.isloadingMore = false;
            });
        };
    });