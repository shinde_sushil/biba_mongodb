var APP = APP || {};
APP.bitlyLink = 'http://bit.ly/Biba_ChangeTheQuestion';

// function load(){
// // 2. This code loads the IFrame Player API code asynchronously.
//     var tag = document.createElement('script');
//     tag.src = "https://www.youtube.com/iframe_api";
//     var firstScriptTag = document.getElementsByTagName('script')[0];
//     firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
// }
// // 3. This function creates an  (and YouTube player)
// //    after the API code downloads.
// var bannerHeight= $('.banner-wrapper').innerHeight();
// var player;
// function onYouTubeIframeAPIReady() {
//     player = new YT.Player('player', {
//         height: 'bannerHeight',
//         width: '640',
//         videoId: 'I6aXW3V-fpY',
//         events: {
//             'onReady': onPlayerReady,
//             'onStateChange': onPlayerStateChange
//         }
//     });
// }
// // 4. The API will call this function when the video player is ready.
// function onPlayerReady(event) {
//     event.target.playVideo();
// }
// // 5. The API calls this function when the player's state changes.
// //    The function indicates that when playing a video (state=1),
// //    the player should play for six seconds and then stop.
// var done = false;
// function onPlayerStateChange(event) {
//     if (event.data == YT.PlayerState.PLAYING && !done) {
//         // setTimeout(stopVideo, 9000);
//         done = true;
//     }
// }
// function stopVideo() {
//     player.stopVideo();
// }

$(document).ready(function(){
    //colorbox modal

    $("#upload-btn").click(function(){
        $.colorbox({
            rel:'group1',
            transition:"elastic",
            inline:true,
            escKey:true,
            href:"#myModal"
        });
    });

    $("#tnc").click(function(e){
        e.preventDefault();
        $.colorbox({
            transition:"elastic",
            inline:true,
            escKey:true,
            href:"#ruleModal"
        });

    });

    //loadColors();       //load colors to gallery cards

    //modal form text areay length graphics
    $( "#question" ).keyup(function() {
        $(".length").text( $(this).val().length + "/140");
    });

    //mobile navbar
    $('#nav-icon4').click(function(){
        $(this).toggleClass('open');
        $(".navbar").toggleClass('visible');
        //$("")
        $("header").toggleClass('mobile-bar');
    });
    $('.navbar ul li a').click(function () {
        if ($('#nav-icon4').hasClass('open')) {
            $(".navbar").toggleClass('visible');
            $("header").toggleClass('mobile-bar');
            $('#nav-icon4').removeClass('open');
        }
    });

    //scroll to top
    $(document).scroll(function(){
        var fadeTop = $(".main-container").offset().top - 30;
        if($(this).scrollTop() > fadeTop){
            $('header').addClass("fade-down");
        } else{
            $('header').removeClass("fade-down");
        }
        //if(!APP.isMobile == 1){
        //    onScroll();
        //}

    });

    $(".down-arrow").click(function(){
        $('html, body').animate({
            scrollTop: $(".main-container").offset().top
        }, 800);
    });

    //video image click
    $(".play-icon").click(function(){
        var bannerHeight= $('.banner-wrapper').innerHeight();
        $(".img").hide();
        $(this).hide();
        $("iframe").css({'height': bannerHeight, 'display' : 'block'});
        $("iframe").show()[0].src += "&autoplay=1";

    });

    $('#fb').on('click', function () {
        APP.fbfeedShare();
    });

    //fb & tw share
    $('#fb-home').on('click', function () {
       APP.fbShare();
    });

    $('#tw-home').on('click', function () {
        APP.twShare();
    });

    $('#tw').on('click', function () {
        var msg = encodeURIComponent("I have helped #ChangeTheQuestion. You can also join BIBA and make a little change that goes a long way!");
        APP.twShare(APP.bitlyLink, msg);
    });

    (function () {
        // Set the size of the rendered Emojis
        // This can be set to 16x16, 36x36, or 72x72
        twemoji.size = '16x16';
        // Parse the document body and // insert <img> tags in place of Unicode Emojis
        twemoji.parse(document.getElementById('gallery'));
    })();
});

//nav bar
$(document).on('click', 'a.nav', function(e){
    e.preventDefault();
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top - ($("header").height())
    }, 800);


});

//fb feed share
APP.fbfeedShare = function (link) {
    if (!link) {
        link = APP.baseUrl + '/?source=fb';
    }
    FB.ui({
        method: 'share',
        href: link
    }, function(response){});
};

//fb & tw share

APP.fbShare = function (link) {
    if (!link) {
        link = APP.baseUrl;
    }
    FB.ui({
        method: 'share',
        href: link
    }, function (response) {
    });
};


APP.twShare = function (link, message) {
    if (!link) {
        link = APP.bitlyLink;
    }
    if (!message) {
        message = encodeURIComponent(APP.shareCopy);
    }
    window.open('https://twitter.com/share?text=' + message + '&url=' + link, 'twitterwindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
};

function onScroll(event){
    var scrollPosition = $(document).scrollTop();
    $('.nav').each(function () {
        var currentLink = $(this);
        var refElement = $(currentLink.attr("href"));
        if (refElement.position().top <= scrollPosition && refElement.position().top + refElement.height() > scrollPosition) {

            $('nav ul li ').removeClass('active');
            $(this).parent().addClass('active');
        }
        else{
            $(this).parent().removeClass('active');
        }
    });
}






//boilerplate code from suntrust project
/*$(document).ready(function(){
    $('.mobile-toggle').click(function () {

        if ($('header').hasClass('open-nav')) {
            $('header').removeClass('open-nav');
        } else {
            $('header').addClass('open-nav');
        }
    });
    $('nav.main-menu li a').click(function () {
        if ($('header').hasClass('open-nav')) {
            $('header').removeClass('open-nav');
        }
    });

    var header_height=$("header").innerHeight();
    $(".home_banner,.thankyou-section,.winners-section,.register,.register-thankyou,.rules-section").css('margin-top',header_height);
    $(".info-copy a").on('click', function(event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            var top=$(hash).offset().top - header_height/1.5;
            $('html, body').animate({
                scrollTop:top
            },600);
        }
    });


    var jump=function(e)
    {
        if (e){
            e.preventDefault();
            var target = $(this).attr("href");
        }else{
            var target = location.hash;
        }
        $('html,body').animate(
            {
                scrollTop: $(target).offset().top - (header_height+10)
            },800);
    }

    $('.error_msg a[href^=#] ').bind("click", jump);
    if (location.hash) {
        $('html, body').scrollTop(0).show();
        jump();
    }

});*/

