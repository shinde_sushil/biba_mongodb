angular.module('entryController', ['datatables', 'datatables.buttons'])
    .controller('entryIndex', function($scope, $http, DTOptionsBuilder) {
        $scope.rows = {};
        $scope.dataFetched = false;
        $scope.$watchGroup(['rows', 'dataFetched'], function() {

        });

        $scope.dtOptions = DTOptionsBuilder
            .newOptions()
            .withDOM('frtip')
            .withPaginationType('full_numbers')
            .withButtons([
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                'pageLength'
            ])
            .withOption('order', [0, 'desc'])
            .withDisplayLength(25);

        //DTDefaultOptions.setDOM('Brtip').setButtons();

        $http({
            method: 'POST',
            //url: APP.adminUrl + "/entry/datatable",
            url: APP.baseUrl + "/admin/entry/datatable",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'X-CSRF-TOKEN': APP.csrfToken
            }
        }).then(function(response) {
            $scope.rows = response.data.data;
            $scope.dataFetched = true;
        });
    });