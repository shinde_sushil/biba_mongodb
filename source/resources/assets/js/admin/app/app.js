var APP = APP || {};
var adminApp = angular.module('adminApp', []);

adminApp.directive('jqdatetimepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            element.datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                sideBySide: true,
                showClose: true,
            }).on('dp.change', function() {
                ngModelCtrl.$setViewValue(element.val());
                scope.$apply();
            });
        }
    };
}).filter('youtubeEmbedUrl', function ($sce) {
    return function(videoId) {
        return $sce.trustAsResourceUrl('//www.youtube.com/embed/' + videoId);
    };
}).filter('trustedUrl', function ($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
});
