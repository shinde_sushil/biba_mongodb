var APP = APP || {};

APP.getYoutubeVideoIdFromYoutubeUrl = function(youtube_url) {
    var video_id = '';
    var urlParts = youtube_url.split('v=');
    if (urlParts.length > 1) {
        video_id = urlParts[1].split('&')[0];
    }
    return video_id;
}