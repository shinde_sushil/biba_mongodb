$(window).load(function() {
//$(function() {
    var $table = $('#entryListing');

    $table.find('tfoot td').each( function () {
        var allowed_search_titles = ['ID', 'Email','Author' ,'Type','Network','Status','Phone','Post date'];
        var small_boxes = [];
        var title = $table.find('thead th').eq($(this).index()).text();
        if ($.inArray(title, allowed_search_titles) !== -1) {
            if ($.inArray(title, small_boxes) !== -1) {
                $(this).html('<input type="text" size="4" placeholder="' + title + '" />');
            } else {
                $(this).html('<input type="text" size="12" placeholder="' + title + '" />');
            }
        }
    });


    var dataTable = $table.DataTable({
        processing: true,
        serverSide: true,
        order: [[ 0, "desc" ]],
        pageLength: 25,
        paging: true,
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        ajax: {
            url: APP.baseUrl + "/admin/entry/datatable",
            method:'POST',
            headers: {
                'X-CSRF-TOKEN': APP.csrfToken
            }
        },
        columns: [{
            data: '_id',
            "defaultContent": "<button type='button' id='edit-btn' value='edit' title='edit' class='btn btn-warning custom-btn' data-toggle='modal' data-target='#edit-modal'>Edit</button>" +
            "<button type='button' id='delete-btn' value='delete' title='delete' class='btn btn-danger custom-btn'>Delete</button>"
        }, {
            data: 'status',
            render : function (data, type, row, meta )    {
                switch (data){
                    case "approved": return "<button type='button' id='approve-btn' value='approve' title='approve' class='btn custom-btn' disabled>Approve</button>"+ "<button type='button' id='reject-btn' value='reject' title='reject' class='btn btn-danger custom-btn'>Reject</button>"
                        break;
                    case "rejected": return "<button type='button' id='approve-btn' value='approve' title='approve' class='btn btn-success custom-btn'>Approve</button>"+"<button type='button' id='reject-btn' value='reject' title='reject' class='btn custom-btn' disabled>Reject</button>"
                        break;
                    default: return  "<button type='button' id='approve-btn' value='approve' title='approve' class='btn btn-success custom-btn'>Approve</button>" +
                        "<button type='button' id='reject-btn' value='reject' title='reject' class='btn btn-danger custom-btn'>Reject</button>"
                }
            }
        }, {
            data: 'network'
        }, {
            data: 'email',
            "defaultContent": " "

        }, {
            data: 'phone',
            "defaultContent": " "
        }, {
            data: 'comment',
            render: function(data, type, row, meta) {
                return row.network == 'twitter' ? decodeURIComponent(escape(row.comment)) : decodeURIComponent(row.comment);
            }
        }, {
            data: 'author_name'
        }, {
            data: 'post_date',
            "defaultContent": " ",
            render: function(data, type, row) {
                return row.post_date;
            }

        }, {
            data: 'edit',
            "defaultContent": "<button type='button' id='edit-btn' value='edit' title='edit' class='btn btn-warning custom-btn' data-toggle='modal' data-target='#edit-modal'>Edit</button>" +
            "<button type='button' id='delete-btn' value='delete' title='delete' class='btn btn-danger custom-btn'>Delete</button>"
        }
        ],
        dom: 'Brtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'pageLength'
        ]
    });

    //console.log(dataTable.columns().data('network'));

    // Apply the filter
    dataTable.columns().every( function () {
        var column = this;
        $( 'input', this.footer() ).on( 'keyup', function () {
            column
                .search( this.value )
                .draw();
        });
    });



    // $table.DataTable({
    //     "columnDefs": [ {
    //         "targets": ['Status'],
    //         "render" : function (data, type, row, meta ) {
    //                 switch (data){
    //                     case "approved": return "<button type='button' id='approve-btn' value='approve' title='approve' class='btn btn-success custom-btn' disabled>Approve</button>"
    //                     break;
    //                     case "rejected": return   "<button type='button' id='reject-btn' value='reject' title='reject' class='btn btn-danger custom-btn' disabled>Reject</button>"
    //                     break;
    //                     default: return  "<button type='button' id='approve-btn' value='approve' title='approve' class='btn btn-success custom-btn'>Approve</button>" +
    //                         "<button type='button' id='reject-btn' value='reject' title='reject' class='btn btn-danger custom-btn'>Reject</button>"
    //                 }
    //         }
    //     }]
    // });

});

//function decode_utf8(s) {
//    return decodeURIComponent(escape(s));
//}
//
// $(window).load(function() {
//    $('#entryListing').on( 'draw.dt', function () {
//        //alert( 'Table redraw' );
//        twemoji.parse(document.getElementById("entryListing"));
//    });
// });