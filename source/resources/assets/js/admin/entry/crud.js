/*$(document).ready(function(){
    $(".approve-btn").click(function(e) {
        e.preventDefault();
        var id = $(this).closest("tr")
            .find(".id")
            .text();
        var formData = new FormData();
        formData.append('status', 'approved');
        formData.append('id', id);

        $.ajax({
            url: APP.baseUrl + "/admin/entry/update",
            type: 'POST',
            data: formData,
            async: true,
            headers: {
                'X-CSRF-TOKEN': APP.csrfToken
            },
            success: function (data) {
                alert(data);
                $(this).removeClass('btn-success').prop('disabled', true);
            },
            error: function (response) {
                var errors = response.responseJSON;
                alert(JSON.stringify(errors['errors']));

            },
            complete: function () {
            },
            cache: false,
            contentType: false,
            processData: false

        });
    });


    $(".reject-btn").click(function(e) {
        e.preventDefault();
        var id = $(this).closest("tr")
            .find(".id")
            .text();
        var formData = new FormData();
        formData.append('status', 'rejected');
        formData.append('id', id);

        $.ajax({
            url: APP.baseUrl + "/admin/entry/update",
            type: 'POST',
            data: formData,
            async: true,
            headers: {
                'X-CSRF-TOKEN': APP.csrfToken
            },
            success: function (data) {
                alert(data);
               $(this).removeClass('btn-danger').prop('disabled', true);
            },
            error: function(response){
                var errors = response.responseJSON;
                alert(JSON.stringify(errors['errors']));

            },
            complete: function() {
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
});*/

var clicked = false, clickY,clickX;
$(document).on({
    'mousemove': function(e) {
        clicked && updateScrollPos(e);
    },
    'mousedown': function(e) {
        clicked = true;
        clickY = e.pageY;
    },
    'mouseup': function() {
        clicked = false;
    }
});

var updateScrollPos = function(e) {
    $('html').css('cursor', 'move');
    $(window).scrollTop($(window).scrollTop() + (clickY - e.pageY));
}

$(window).load(function() {
    $('#entryListing tbody').on( 'click', '#approve-btn', function () {
        var table = $('#entryListing').DataTable();
        var data =  table.row( $(this).parents('tr') ).data();

        me =  $(this);
        if (me.data('requestRunning') ) {
            return;
        }

        me.data('requestRunning', true);
        var formData = new FormData();

        for ( var key in data ) {
            formData.append(key, data[key]);
        }
        formData.append('status', 'approved');

        $.ajax({
            url: APP.baseUrl + "/admin/entry/update",
            type: 'POST',
            data: formData,
            async: true,
            headers: {
                'X-CSRF-TOKEN': APP.csrfToken
            },
            success: function (data) {
                alert(data);
                me.data('requestRunning', false).removeClass('btn-success').prop('disabled', true).parents('tr td').children("button#reject-btn").addClass("btn-danger").prop("disabled",false);
            },
            error: function(response){
                var errors = response.responseJSON;
                alert(JSON.stringify(errors['errors']));
                me.data('requestRunning', false);
                me.prop('disabled',false);
            },
            complete: function() {
                me.data('requestRunning', false);
            }   ,
            cache: false,
            contentType: false,
            processData: false
        });
    });

    $('#entryListing tbody').on( 'click', '#reject-btn', function () {
        var table = $('#entryListing').DataTable();
        var data =  table.row( $(this).parents('tr') ).data();
        me =  $(this);
        if (me.data('requestRunning') ) {
            return;
        }
        me.data('requestRunning', true);
        var formData = new FormData();

        for ( var key in data ) {
            formData.append(key, data[key]);
        }
        formData.append('status', 'rejected');

        $.ajax({
            url: APP.baseUrl + "/admin/entry/update",
            type: 'POST',
            data: formData,
            async: true,
            headers: {
                'X-CSRF-TOKEN': APP.csrfToken
            },
            success: function (data) {
                alert(data);
                me.data('requestRunning', false).removeClass('btn-danger').prop('disabled', true).parents('tr td').children("button#approve-btn").addClass("btn-success").prop("disabled",false);
            },
            error: function(response){
                var errors = response.responseJSON;
                alert(JSON.stringify(errors['errors']));
                me.data('requestRunning', false);
            },
            complete: function() {
                me.data('requestRunning', false);
            }   ,
            cache: false,
            contentType: false,
            processData: false
        });
    });

    $('#entryListing tbody').on( 'click', '#delete-btn', function () {
        var result = confirm("Do you want to delete this entry?");
        if(result){
            var table = $('#entryListing').DataTable();
            var data =  table.row( $(this).parents('tr') ).data();
            me =  $(this);
            if (me.data('requestRunning') ) {
                return;
            }
            me.prop('disabled', true);
            me.data('requestRunning', true);
            var formData = new FormData();

            for ( var key in data ) {
                formData.append(key, data[key]);
            }

            $.ajax({
                url: APP.baseUrl + "/admin/entry/destroy",
                type: 'POST',
                data: formData,
                async: true,
                headers: {
                    'X-CSRF-TOKEN': APP.csrfToken
                },
                success: function (data) {
                    alert(data);
                    me.prop('disabled',false);
                    me.data('requestRunning', false);
                    // location.reload();
                    table.ajax.reload();
                },
                error: function(response){
                    var errors = response.responseJSON;
                    alert(JSON.stringify(errors['errors']));
                    me.data('requestRunning', false);
                    me.prop('disabled',false);
                },
                complete: function() {
                    me.data('requestRunning', false);
                    me.prop('disabled',false);
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });

    $('#entry-modal').on('hidden.bs.modal', function () {
        $('#errors').empty();
    });

    $('#edit-modal').on('hidden.bs.modal', function () {
        $('#edit-errors').empty();
    });


    $( "#entry-model-form" ).submit(function(e) {
        e.preventDefault();
        var table = $('#entryListing').DataTable();
        $("#form-submit-btn").prop('disabled', true);
        me=$(this);
        if ( me.data('requestRunning') ) {
            return;
        }
        me.data('requestRunning', true);
        var formData = new FormData(this);
        $.ajax({
            url: APP.baseUrl + "/admin/entry/store",
            type: 'POST',
            data: formData,
            async: true,
            headers: {
                'X-CSRF-TOKEN': APP.csrfToken
            },
            success: function (data) {
                $('#entry-modal').modal('toggle');
                document.getElementById('entry-model-form').reset()
                alert(data);
                $("#form-submit-btn").prop('disabled',false);
                me.data('requestRunning', false);
                table.ajax.reload();
            },
            error: function(response){
                if (response.status ==422){
                    $errors = response.responseJSON;
                    errorsHtml = '<div class="alert alert-danger"><ul>';
                    if($errors['email']!=undefined){
                        errorsHtml += '<li>' + $errors['email'] + '</li>';
                    }
                    if($errors['phone']!=undefined){
                        errorsHtml += '<li>' + $errors['phone'] + '</li>';
                    }
                    errorsHtml += '</ul></di>';
                    $( '#errors' ).html( errorsHtml );
                }
                me.data('requestRunning', false);
                $("#form-submit-btn").prop('disabled', false);
            },
            complete: function() {
                me.data('requestRunning', false);
                $("#form-submit-btn").prop('disabled', false);
            },
            cache: false,
            contentType: false,
            processData: false
        });
        //return false;
    });


    $('#entryListing tbody').on( 'click', '#edit-btn', function () {
        var table = $('#entryListing').DataTable();
        var data =  table.row( $(this).parents('tr') ).data();
        var me =  $(this);
        $('#edit-network').val(data['network']);
        $("#edit-email").val(data['email']);
        $("#edit-phone").val(data['phone']);
        $("#edit-question").val(data['comment']);
        $("#edit-status").val(data['status']);

        $( "#edit-model-form" ).submit(function(e) {
            e.preventDefault();
            $("#edit-submit-btn").prop('disabled', true);
            me=$(this);
            if ( me.data('requestRunning') ) {
                return;
            }
            me.data('requestRunning', true);
            var formData = new FormData();
            for ( var key in data ) {
                formData.append(key, data[key]);
            }

            formData.set('network',$("#edit-network").val());
            formData.set('email',$("#edit-email").val());
            formData.set('phone',$("#edit-phone").val());
            formData.set('comment',$("#edit-question").val());
            formData.set('status',$("#edit-status").val());

            $.ajax({
                url: APP.baseUrl + "/admin/entry/edit",
                type: 'POST',
                data: formData,
                async: true,
                headers: {
                    'X-CSRF-TOKEN': APP.csrfToken
                },
                success: function (data) {
                    $('#edit-modal').modal('toggle');
                    table.ajax.reload();
                    $("#form-submit-btn").prop('disabled',false);
                    alert(data);
                    me.data('requestRunning', false);
                },
                error: function(response){
                    if (response.status ==422){
                        $errors = response.responseJSON;
                        errorsHtml = '<div class="alert alert-danger"><ul>';
                        if($errors['email']!=undefined){
                            errorsHtml += '<li>' + $errors['email'] + '</li>';
                        }
                        if($errors['phone']!=undefined){
                            errorsHtml += '<li>' + $errors['phone'] + '</li>';
                        }
                        errorsHtml += '</ul></di>';
                        $( '#edit-errors' ).html( errorsHtml );
                    }

                    me.data('requestRunning', false);
                    $("#edit-submit-btn").prop('disabled', false);
                },
                complete: function() {
                    me.data('requestRunning', false);
                    $("#edit-submit-btn").prop('disabled', false);
                },
                cache: false,
                contentType: false,
                processData: false
            });
        });
    });
});
